<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('description')->nullable($value = true);
            $table->integer('role_id');
            $table->integer('part_id');
            $table->integer('login_count')->default(0);
            $table->integer('deleted')->default(0)->comment('0:not deleted; 1:deleted');
            $table->integer('activate')->default(0)->comment('0:not activate; 1:activate');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
