<?php
/**
 * Controller used to manage users of the site.
 *
 * @Route("user/listUser")
 * @
 * @author quandv <quandv@rikkeisoft.com>
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\User;
use App\Role;
use App\Part;

class UserController extends Controller
{
    public function login()
    {
    	return view('users.login');
    }

    public function postLogin(Request $request)
    {
    	$this->validate($request,[
    		'email'=>'required',
    		'password'=>'required|min:3|max:32',
    	],[
    		'email.required'=>'Please input email',
    		'password.required'=>'Please input password',
    		'password.min'=>'Password must be >= 3 characters',
    		'password.max'=>'Password must be <= 32 characters',
    	]);

    	if (Auth::attempt(['email'=>$request->email,'password'=>$request->password])) {
    		return redirect('user/listUser');
    	} else {
    		return redirect('user/login')->with('notices','Login failed!');
    	}
    }

    public function logout()
    {
    	Auth::logout();
		return redirect('user/login');
    }

    public function listUser()
    {
    	$users = User::all();
    	return view('users.listUser',compact('users'));
    }

    public function addUser()
    {
    	$roles = Role::all();
    	$parts = Part::all();
    	return view('users.addUser',compact('roles','parts'));
    }

	public function postAddUser(Request $request)
    {
		$this->validate($request,
    		[
                'name'=>'required|min:3',
    			'email'=>'required|email|unique:users,email',
    			'password'=>'required|min:3|max:32',
    			'confirm_password'=>'required|same:password'
    		],
    		[
                'name.required'=>'Please input name',
                'name.min'=>'Name must be <= 3 characters',
    			'email.required'=>'Please input email',
    			'email.email'=>'Format email incorrect',
    			'email.unique'=>'Email is not existing',
    			'password.unique'=>'Please input password',
    			'password.min'=>'Password must be >= 3 characters',
    			'password.max'=>'Password must be <= 32 characters',
    			'confirm_password.unique'=>'Please input confirm_password',
    			'confirm_password.same'=>'Confirm_password do not match with password',
    		]);
    	$user=  new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->role_id = $request->role_id;
        $user->part_id = $request->part_id;
    	$user->save();
    	return redirect('user/addUser')->with('notices','Add user failed!');
    }
}
